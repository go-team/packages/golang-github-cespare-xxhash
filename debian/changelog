golang-github-cespare-xxhash (2.3.0-1) unstable; urgency=medium

  [ Anton Gladky ]
  * [d3504b8] Update standards version to 4.6.2, no changes needed.
  * [33818ec] Set debhelper-compat version in Build-Depends.
  * [9be8029] Bump debhelper from old 12 to 13.
  * [f7ca991] Remove empty debian/patches/series.
  * [89f16e1] Update watch file format version to 4.

  [ Anthony Fok ]
  * New upstream version 2.3.0
  * Split lines in debian/watch for easier reading
  * Reorder fields in debian/control and debian/copyright
  * Change Section from devel to golang
  * Update Maintainer email address to team+pkg-go@tracker.debian.org
  * Add myself to the list of Uploaders
  * Use dh-sequence-golang instead of dh-golang and --with=golang
  * Invoke dh with --builddirectory=_build
  * Add "Rules-Requires-Root: no" to debian/control
  * Bump Standards-Version to 4.7.0 (no change)
  * Drop -dev package’s unused dependency on ${shlibs:Depends}
  * Clarify that this package is a "Go" implementation in the synopsis
  * Add Upstream-Contact field in debian/copyright
  * Add debian/upstream/metadata file
  * Add "dynamic" and "xxhsum" to DH_GOLANG_EXCLUDES
  * debian/gbp.conf: Set debian-branch to debian/sid for DEP-14 conformance

 -- Anthony Fok <foka@debian.org>  Sat, 10 Aug 2024 04:14:57 -0600

golang-github-cespare-xxhash (2.1.1-2) unstable; urgency=medium

  * Team upload.
  * Mark golang-github-cespare-xxhash-dev Multi-Arch: foreign. (Closes: #984649)
    Thanks to Helmut Grohne <helmut@subdivi.de> for the patch

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 28 Aug 2021 16:05:44 +0200

golang-github-cespare-xxhash (2.1.1-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version: 4.4.1.
  * Added myself to Uploaders (Closes: #940355).

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 16 Dec 2019 12:56:20 +1100

golang-github-cespare-xxhash (2.1.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Standards-Version: 4.4.0.
  * DH & compat to version 12.

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 09 Sep 2019 12:21:31 +1000

golang-github-cespare-xxhash (1.1.0-1) unstable; urgency=medium

  * New upstream version.

 -- Alexandre Viau <aviau@debian.org>  Sat, 17 Nov 2018 12:41:20 -0500

golang-github-cespare-xxhash (1.0.0-3) unstable; urgency=medium

  * Team upload.
  * Vcs-* urls: pkg-go-team -> go-team.

 -- Alexandre Viau <aviau@debian.org>  Tue, 06 Feb 2018 00:05:42 -0500

golang-github-cespare-xxhash (1.0.0-2) unstable; urgency=medium

  * point Vcs-* urls to salsa.d.o subproject

 -- Alexandre Viau <aviau@debian.org>  Thu, 25 Jan 2018 16:41:31 -0500

golang-github-cespare-xxhash (1.0.0-1) unstable; urgency=medium

  * Move to salsa.debian.org.
  * Testsuite: autopkgtest-pkg-go.
  * Priority extra -> optional.
  * New upstram version.

 -- Alexandre Viau <aviau@debian.org>  Sat, 30 Dec 2017 14:18:12 -0500

golang-github-cespare-xxhash (0.0~git20170604.0.1b6d2e4-1) unstable; urgency=medium

  * Initial release (Closes: #865077)

 -- Alexandre Viau <aviau@debian.org>  Mon, 19 Jun 2017 01:25:51 -0400
